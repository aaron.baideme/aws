image:
  name: hashicorp/terraform:light
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

# Default output file for Terraform plan
variables:
  PLAN: plan.tfplan
  JSON_PLAN_FILE: tfplan.json
  TF_IN_AUTOMATION: "true"

cache:
  key: "$CI_COMMIT_REF_SLUG"
  paths:
    - .terraform

before_script:
  - apk add --update curl jq
  - alias convert_report="jq -r '([.resource_changes[].change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
  - curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/kubectl
  - install kubectl /usr/local/bin/ && rm kubectl
  - curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator
  - install aws-iam-authenticator /usr/local/bin/ && rm aws-iam-authenticator
  - terraform --version
  - terraform init

stages:
  - validate
  - plan
  - apply
  - deploy
  - destroy

validate:
  stage: validate
  script:
    - terraform validate
    - terraform fmt -check=true
  only:
    - branches

tfsec:
  image: 
    name: wesleydeanflexion/tfsec
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/go/bin'
  stage: plan
  before_script:
    - tfsec -v
  script: tfsec . -f json | tee gl-sast-report.json
  artifacts:
    reports:
      sast: gl-sast-report.json

merge review:
  stage: plan
  script:
    - terraform plan -out=$PLAN
    - "terraform show --json $PLAN | convert_report > $JSON_PLAN_FILE"
    - echo \`\`\`diff > plan.txt
    - terraform show -no-color ${PLAN} | tee -a plan.txt
    - echo \`\`\` >> plan.txt
    - sed -i -e 's/  +/+/g' plan.txt
    - sed -i -e 's/  ~/~/g' plan.txt
    - sed -i -e 's/  -/-/g' plan.txt
    - MESSAGE=$(cat plan.txt)
    - >-
      curl -X POST -g -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" 
      --data-urlencode "body=${MESSAGE}" 
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/discussions"
  artifacts:
    name: plan
    paths:
      - $PLAN
    reports:
        terraform: $JSON_PLAN_FILE
  only:
    - merge_requests

plan production:
  stage: plan
  script:
    - terraform plan -out=$PLAN
  artifacts:
    name: plan
    paths:
      - $PLAN
  only:
    - master
  resource_group: production

apply:
  stage: apply
  script:
    - terraform apply -input=false $PLAN
    - DYNAMIC_ENVIRONMENT_URL=$(terraform output -no-color env-dynamic-url)
    - echo "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL" >> deploy.env
  dependencies:
    - plan production
  artifacts:
    name: $CI_COMMIT_REF_SLUG
    untracked: true
    reports:
      dotenv: deploy.env
  only:
    - master
  resource_group: production
  environment:
    name: production
    url: $DYNAMIC_ENVIRONMENT_URL
    on_stop: destroy

deploy-apps:
  stage: deploy
  variables:
    APP_ENVIRONMENT: $CI_PROJECT_NAME
  trigger: magnetic-asia/apps/cluster-management
  only:
    - master

destroy:
  stage: destroy
  script:
    - terraform destroy -auto-approve
  when: manual
  only:
    - master  
  environment:
    name: production
    action: stop
    deploy.env