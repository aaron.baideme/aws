terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "magnetic-asia"

    workspaces {
      name = "aws-example-gitlab"
    }
  }
}